Android KVM
===========
Run [Genymotion](http://www.genymotion.com/) Android VirtualBox image with Qemu
KVM.

Genymotion Tech Info: [AndroVM –
Documentation](http://androvm.org/blog/androvm-documentation/)

The Qemu KVM feels faster and more stable than VirtualBox. Qemu also feels less
resource hungry.

Usage
=====
Create a network bridge over local machine:

*   On Debian:
    Edit /etc/network/interfaces, add the following lines:
    
        auto br0
        iface br0 inet dhcp
           pre-up ip tuntap add dev tap0 mode tap
           pre-up ip link set tap0 up
           bridge_ports all tap0
           bridge_stp off
           bridge_maxwait 0
           bridge_fd      0
           post-down ip link set tap0 down
           post-down ip tuntap del dev tap0 mode tap

    Save and restart network:

        $ sudo service networking restart

*   Create Android VM using [genymotion](http://www.genymotion.com/).

*   Go to directory with created virtual machine images 

        $ cd ~/.Genymobile/Genymotion/deployed/some-android-vm/
        $ android-kvm -c
        $ android-kvm

      `some-android-vm` is the VM directory you created with genymotion.

    The command `android-kvm -c` will convert VirtualBox/VMware disk images to
    the "qcow2" format of Qemu.

Known Issues
============
* OpenGL is not supported by Qemu. Any OpenGL calls will break.

